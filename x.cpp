#include <X11/Xlib.h>
#include <assert.h>
#include <memory>
#include <unistd.h>

int main(){
    Display *dpy = XOpenDisplay(NULL);
    assert(dpy);

    int blackColor = BlackPixel(dpy, DefaultScreen(dpy));
    int whiteColor = WhitePixel(dpy, DefaultScreen(dpy));

    // Create the window
    Window window = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, 200, 100,
                                   0, blackColor, blackColor);

    // Get MapNotify events
    XSelectInput(dpy, window, StructureNotifyMask);
    // Map the window - appear on the screen
    XMapWindow(dpy, window);

    GC graphicsContext = XCreateGC(dpy, window, 0, NULL);

    // Drawing using white color
    XSetForeground(dpy, graphicsContext, whiteColor);

    // wait for MapNotify event
    while(1){
        XEvent e;
        XNextEvent(dpy, &e);
        if (e.type == MapNotify)
            break;
    }

    // Draw the line
    XDrawLine(dpy, window, graphicsContext, 10, 60, 180, 20);

    // Send request to server
    XFlush(dpy);

    sleep(3);

    return 0;
}
